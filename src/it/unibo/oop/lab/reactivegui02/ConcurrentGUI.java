package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI extends JFrame{
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private static final double WIDTH_PERC = 0.2;
    private static final double HEIGHT_PERC = 0.1;
    private final JLabel label = new JLabel();
    
    public ConcurrentGUI () {
        super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel mainPanel = new JPanel(new FlowLayout());
        this.setContentPane(mainPanel);
        final Agent agent = new Agent();
        new Thread(agent).start();
        
        final JButton up = new JButton("up");
        up.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setFlag(true);
            }
        });
        final JButton down = new JButton("down");
        down.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.setFlag(false);
            }
        });
        final JButton stop = new JButton("stop");
        stop.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                agent.stopCounting();
                up.setEnabled(false);
                down.setEnabled(false);
                stop.setEnabled(false);
            }
        });
        mainPanel.add(label);
        mainPanel.add(up);
        mainPanel.add(down);
        mainPanel.add(stop);
        
        this.setVisible(true);
    }
    
    private class Agent implements Runnable{

        private volatile boolean stop;
        private int counter;
        private boolean flag;
        
        @Override
        public void run() {
            while(!this.stop) {
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {
                        @Override
                        public void run() {
                            ConcurrentGUI.this.label.setText(Integer.toString(counter));
                        }
                    });
                    changeCounter();
                    Thread.sleep(100);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        public void setFlag(final boolean flag) {
            this.flag = flag;
        }
        
        public void stopCounting() {
            this.stop = true;
        }
        
        private void changeCounter() {
            if(flag) {
                this.counter++;
            }
            else {
                this.counter--;
            }
        }
        
    }
}
