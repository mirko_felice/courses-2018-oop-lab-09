package it.unibo.oop.lab.workers02;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * 
 *
 */
public class MultiThreadedSumMatrix implements SumMatrix {

    private final int nThreads;

    /**
     * 
     * @param nThreads the number of threads the client will use.
     */
    public MultiThreadedSumMatrix(final int nThreads) {
        this.nThreads = nThreads;
    }

    private static class Worker extends Thread {

        private final double[][] matrix;
        private final int start;
        private final int end;
        private double res;

        /**
         * 
         * @param matrix the matrix to sum.
         * @param start the starting row.
         * @param end the ending row.
         */
        Worker(final double[][] matrix, final int start, final int end) {
            this.matrix = Arrays.copyOf(matrix, matrix.length);
            this.start = start;
            this.end = end;
        }

        public void run() {
            System.out.println("Working from position [" + this.start + "] to position [" + this.end + "]");
            for (int r = start; r < end && r < this.matrix.length; r++) {
                for (final double d: matrix[r]) {
                    this.res += d;
                }
            }
        }

        public double getResult() {
            return this.res;
        }
    }

    /**
     * 
     */
    @Override
    public double sum(final double[][] matrix) {
        final int size = matrix.length % this.nThreads + matrix.length / this.nThreads;
        final List<Worker> workers = new ArrayList<>(this.nThreads);
        for (int start = 0; start < matrix.length; start += size) {
            workers.add(new Worker(matrix, start, start + size));
        }

        for (final Worker w: workers) {
            w.start();
        }

        double sum = 0;
        for (final Worker w: workers) {
            try {
                w.join();
                sum += w.getResult();
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
        return sum;
    }

}
